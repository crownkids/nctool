<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings.
 *
 * @package   local_nctool
 * @copyright 2016, Vyacheslav Strelkov <strelkov.vo@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Create folder / submenu in block menu, modsettings for activity modules, localplugins for Local plugins.
// The default folders are defined in admin/settings/plugins.php.
$ADMIN->add('nctoolsettings', new admin_category('nctooldirectory', get_string('navblockmenu', 'local_nctool')));

// This adds a link to an external page.
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade1', get_string('navblockgrade1', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade2', get_string('navblockgrade2', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade3', get_string('navblockgrade3', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade4', get_string('navblockgrade4', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade5', get_string('navblockgrade5', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade6', get_string('navblockgrade6', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade7', get_string('navblockgrade7', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));
$ADMIN->add('nctooldirectory', new admin_externalpage('navblockgrade8', get_string('navblockgrade8', 'local_nctool'),
    $CFG->wwwroot.'/local/nctool/index.php'));

$settings = null;