<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   local_nctool
 * @copyright 2016, Vyacheslav Strelkov <strelkov.vo@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Lesson class.
 *
 * Represents an instance of fully converted 'oks' object.
 *
 * @package   local_nctool
 * @copyright 2016, Vyacheslav Strelkov <strelkov.vo@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class lesson {
    /** @var int Lesson id as presented in database */
    private $id;

    /**  @var string Lesson title in original language */
    private $originaltitle;

    /** @var string Lesson title translated */
    private $translatedtitle;

    /** @var string Type of educational organization */
    private $organizationtype;

    /** @var int Lesson grade */
    private $grade;

    /** @var int Lesson order number */
    private $lessonnumber;

    /** @var int Course section id corresponding to this lesson */
    private $coursesection;
    
    /** @var array Array of content sections */
    private $sections;
    
    /** @var int Oks file id */
    private $fileid;

    /** @var int Timestamp of last editing */
    private $timemodified;

    public function get_id() {
        return $this->id;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function get_original_title() {
        return $this->originaltitle;
    }
    
    public function set_original_title($originaltitle) {
        $this->originaltitle = $originaltitle;
    }
    
    public function get_translated_title() {
        return $this->translatedtitle;
    }
    
    public function set_translated_title($translatedtitle) {
        $this->translatedtitle = $translatedtitle;
    }
    
    public function get_organization_type() {
        return $this->organizationtype;
    }
    
    public function set_organization_type($organizationtype) {
        $this->organizationtype = $organizationtype;
    }
    
    public function get_grade() {
        return $this->grade;
    }
    
    public function set_grade($grade) {
        $this->grade = $grade;
    }

    public function get_lesson_number() {
        return $this->lessonnumber;
    }

    public function set_lesson_number($lessonnumber) {
        $this->lessonnumber = $lessonnumber;
    }
    
    public function get_course_section() {
        return $this->coursesection;
    }

    public function set_course_section($coursesection) {
        $this->coursesection = $coursesection;
    }
    
    public function get_sections() {
        return $this->sections;
    }
    
    public function set_sections($sections) {
        $this->sections = $sections;
    }
    
    public function get_file_id() {
        return $this->fileid;
    }
    
    public function set_file_id($fileid) {
        $this->fileid = $fileid;
    }
    
    public function get_timemodified() {
        return $this->timemodified;
    }

    public function set_timemodified($timemodified) {
        $this->timemodified = $timemodified;
    }
}