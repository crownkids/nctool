<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   plugintype_pluginname
 * @copyright 2016, Vyacheslav Strelkov <strelkov.vo@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Lesson class section.
 *
 * @package   local_nctool
 * @copyright 2016, Vyacheslav Strelkov <strelkov.vo@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class lesson_section {
    /** @var int Lesson section id as presented in database */
    private $id;

    /** @var int Corrseponding lesson id */
    private $lessonid;

    /** @var int Title in original language */
    private $originaltitle;

    /** @var int Translated title */
    private $translatedtitle;

    /** @var int Section type. Can be 'slides', 'phrases' */
    private $type;

    /** @var int Order number in lesson */
    private $ordernumber;
    
    /** @var array Array of content items */
    private $content;
    
    /** @var int Timestamp of last editing */
    private $timemodified;

    public function get_id() {
        return $this->id;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function get_lessonid() {
        return $this->lessonid;
    }

    public function set_lessonid($lessonid) {
        $this->lessonid = $lessonid;
    }
    
    public function get_originaltitle() {
        return $this->originaltitle;
    }

    public function set_originaltitle($originaltitle) {
        $this->originaltitle = $originaltitle;
    }
    
    public function get_translatedtitle() {
        return $this->translatedtitle;
    }
    
    public function set_translatedtitle($translatedtitle) {
        $this->translatedtitle = $translatedtitle;
    }
    
    public function get_type() {
        return $this->type;
    }
    
    public function set_type($type) {
        $this->type = $type;
    }
    
    public function get_ordernumber() {
        return $this->ordernumber;
    }
    
    public function set_ordernumber($ordernumber) {
        $this->ordernumber = $ordernumber;
    }

    public function get_content() {
        return $this->content;
    }

    public function set_content($content) {
        $this->content = $content;
    }
    
    public function get_timemodified() {
        return $this->timemodified;
    }
    
    public function set_timemodified($timemodified) {
        $this->timemodified = $timemodified;
    }
}