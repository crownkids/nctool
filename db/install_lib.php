<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   local_nctool
 * @copyright 2016, Vyacheslav Strelkov <strelkov.vo@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 *  Creates proper directory structure in moodledata directory after installation.
 */
function create_directory_structure()
{
    global $CFG;

    $rootpath = $CFG->dataroot . '/nclessons';
    mkdir_if_not_exist($rootpath);

    $gradepathappendix = [
        'k01',
        'k02',
        's01',
        's02',
        's03',
        's04',
        's05',
        's06',
        's07',
        's08',
        's09',
        's10',
        's11',
    ];

    foreach ($gradepathappendix as $gradepathappendix) {
        $pathtogradedir = $rootpath . '/' . $gradepathappendix;
        mkdir_if_not_exist($pathtogradedir);

        $i = 1;
        while ($i < 73) {
            $pathtolessondir = $pathtogradedir . '/' . strval($i);
            mkdir_if_not_exist($pathtolessondir);
        }
    }
}

/**
 *  Creates directory if one does not exist.
 */
function mkdir_if_not_exist($path) {
    if ( !is_dir($path) ) {
        mkdir($path);
    }
}